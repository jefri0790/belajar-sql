-- Soal 1 Membuat Database
-- CREATE DATABASE myshop

CREATE DATABASE myshop;

-- Soal 2 Membuat Table dalam database
-- CREATE TABLE USER,ITEMS,CATEGORIES

CREATE TABLE `USER`
(
    ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    NAME varchar(255),
    EMAIL varchar(255),
    PASSWORD varchar(255)
);

CREATE TABLE `CATEGORIES`
(
     ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
     NAME varchar(255)
);

CREATE TABLE `ITEMS`
(
    ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    NAME varchar(255),
    DESCRIPTION varchar(255),
    PRICE int,
    STOCK int,
    CATEGORY_ID int ,
    FOREIGN KEY(CATEGORY_ID) REFERENCES CATEGORIES(ID)
);


-- SOAL 3 - Memasukkan data pada Table
-- Insert rows into table 'USER'
INSERT INTO USER
( 
    NAME,EMAIL,PASSWORD
)
VALUES
( 
    'JOHN DOE','John@Doe.com','John123'
),
(
    'Jane DOE','Jane@Doe.com','Jenita123'
);


-- Insert rows into table 'CATEGORIES'
INSERT INTO CATEGORIES
( 
    NAME
)
VALUES
('GADGET'),('CLOTH'),('MEN'),('WOMEN'),('BRANDED');


-- Insert rows into table 'ITEMS'
INSERT INTO ITEMS
(
    NAME,DESCRIPTION,PRICE,STOCK,CATEGORY_ID
)
VALUES
(
    'SUMSANG B50','Hape Keren dari merek Sumsang',4000000,100,1
),
(
    'UNIKLOOH','Baju Keren dari Brand Ternama',500000,50,2
),
(
    'IMHO WATCH','Jam Tangan Anak yang jujur banget',2000000,10,1
);


--SOAL 4 Mengambil data dari database
--A Mengambil data User

Select ID, Name,Email From USER;

--B Mengambil Data Items

SELECT * from items where price >1000000;
SELECT * from items where name like '%UNIKLO%' or name like '%WATCH%' or name like '%SANG%'; 
SELECT * from items where name like '%UNIKLO%';
SELECT * from items where name like '%WATCH%';
SELECT * from items where name like '%SANG%';

-- C Menampilka data items join dengan kategori

SELECT i.Name,DESCRIPTION,PRice,stock,category_ID,c.name as kategori
from items i 
Join categories c on i.category_id=c.id;

-- SOAL 5 Mengubah data dari database
-- Update rows in table 'ITEMS'
UPDATE ITEMS
SET
   PRICE = 2500000
WHERE 
    NAME LIKE '%SUMSANG%';

